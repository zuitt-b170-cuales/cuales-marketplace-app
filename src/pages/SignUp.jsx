import React from 'react'
import CommonSection from '../components/ui/Common-section/CommonSection'
import SignupSection from '../components/ui/SignUp/SignupSection'

const SignUp = () => {
	return (
		<>
		<SignupSection />
		</>
	);
};

export default SignUp;
import React from 'react'

import { Routes, Route, Navigate } from 'react-router-dom'
import ScrollToTop from '../components/Functionality/ScrollToTop/ScrollToTop'

import Home from '../pages/Home'
import Market from '../pages/Market'
import Create from '../pages/Create'
import Contact from '../pages/Contact'
import SellerProfile from '../pages/SellerProfile'
import EditProfile from '../pages/EditProfile'
import Wallet from '../pages/Wallet'
import ItemDetails from '../pages/ItemDetails'
import History from '../pages/History'
import Login from '../pages/Login'
import SignUp from '../pages/SignUp'

const Routers = () => {
	return ( 
	<ScrollToTop>
	<Routes>
		<Route path='/' element={<Navigate to = '/home' />} />
		<Route path='/home' element={<Home />} />
		<Route path='/market' element={<Market />} />
		<Route path='/create' element={<Create />} />
		<Route path='/contact' element={<Contact />} />
		<Route path='/edit-profile' element={<EditProfile />} />
		<Route path='/seller-profile' element={<SellerProfile />} />
		<Route path='/wallet' element={<Wallet />} />
		<Route path='/market/:id' element={<ItemDetails />} />
		<Route path='/history' element={<History />} />
		<Route path='/login' element={<Login />} />
		<Route path='/signup' element={<SignUp />} />
	</Routes>
	</ScrollToTop>
)}

export default Routers
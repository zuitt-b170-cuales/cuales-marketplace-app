import React from 'react'
import { Container, Row, Col } from 'reactstrap'
import { Link } from 'react-router-dom'

import ProductCard from "../Product-card/ProductCard"
import { ITEM__DATA } from '../../../assets/data/data.js'

import './live-auction.css'

// FIX FORMATTING: Created by and Current Bid.
// FIX HOVERING: Explore more

const LiveAuction = () => {
	return <section>
		<Container>
			<Row>
				<Col lg='12' className='mb-5'>
					<div className="live__auction__top d-flex align-items-center justify-content-between">
						<h3>Live Auction</h3>
						<span className="explore__link"><Link to='/market'>Explore more</Link></span>
					</div>
				</Col>
				{
					ITEM__DATA.slice(0,4).map((item) => (
						<Col lg='3' md='4' sm='6' className="mb-4">
						<ProductCard key={item.id} item={item} />
						</Col>
					))
				}
			</Row>
		</Container>
	</section>
}


export default LiveAuction
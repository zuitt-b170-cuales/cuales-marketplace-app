import React from 'react'
import { Container, Row, Col } from 'reactstrap'
import { Link } from 'react-router-dom'
import './hero-section.css'

// import heroImg from '../../assets/images/hero.jpg'
import heroImg from '../../assets/images/test.png'

const HeroSection = () => {
	return (
	<section className="hero__section">
		<Container>
			<Row>
				<Col lg='6' md='6' sm='6' className="d-flex align-items-center order-lg-2 order-sm-1 order-xs-1">
					<div className="hero__img d-flex align-items-center">
						<img src={heroImg} alt="" className="w-100" />
					</div>
				</Col>
				<Col lg='6' md='6' sm='6' order-sm='2' className="order-lg-1 order-sm-2 order-xs-2">
					<div className="hero__content">
						<h2>Discover, collect, and trade <span>limited physical assets</span> from your favorite brands, people, and more.</h2>
						<p>Supafaya is the world's first and largest limited physical assets marketplace</p>

						<div className="hero__btns d-flex align-items-center gap-4">
							<Link to='/market'><button className="explore__btn d-flex align-items-center gap-2"><i class="ri-rocket-2-line"></i>Explore</button></Link>
							<Link to='/create'><button className="create__btn d-flex align-items-center gap-2"><i class="ri-ball-pen-line"></i>Featured</button></Link>
						</div>
					</div>
				</Col>
			</Row>
		</Container>
	</section>
)}

export default HeroSection
import React from 'react'
import { Container, Row, Col } from 'reactstrap'
import { Link } from 'react-router-dom'

import './ad-section.css'
import adImg from '../../../assets/images/ad.png'

// FIX FORMATTING: Created by and Current Bid.
// FIX HOVERING: Explore more

const AdSection = () => {
	return <section className="d-none d-lg-block d-xl-block">
		<Container>
			<Row>
				<Col lg='12' className='mb-5'>
					<Link to="/contact">
						<div className="adImg">
							<img src={adImg} alt="" className="w-100" />
						</div>
					</Link>
				</Col>
			</Row>
		</Container>
	</section>
}


export default AdSection